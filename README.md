api.js
============

[![Codeship Status for k0tt/api.js](https://app.codeship.com/projects/82cd2390-2f1d-0137-9854-76346f9ba732/status?branch=master)](https://app.codeship.com/projects/331777)<Paste>

[![codecov](https://codecov.io/gl/k0tt/api.js/branch/master/graph/badge.svg)](https://codecov.io/gl/k0tt/api.js)




api.js is a REST helper library for javascript. The purpose is to provide methods that are commonly used when dealing with REST request to and from a server.


### Installation

With bower

	bower install --save-dev apidotjs
	
With npm

	npm install --save-dev apidotjs
	
With yarn

	yarn add apidotjs --dev


### Testing

To run functional tests you need to keep the test server running while executing tests. Run

	npm start
	
To start the test server

##### Starting the test server with Docker
A Dockerfile is included at the root of the project. To start the test server with docker simply run

	docker build -t api .
	docker run --name api -p 8000:8000 -d api npm start
	
	
This command will run unit/functional tests

	gulp test
	

### Documentation

Gitbook - [api.js](https://kottur.gitbooks.io/api-js/content/)

	
### Contribution

Issue tracker: [https://github.com/K0TT/api.js/issues](https://github.com/K0TT/api.js/issues)

Contact: [doddi@kott.is](mailto:doddi@kott.is)

