import gulp from 'gulp';
import codecov from 'gulp-codecov';
 

function uploadTestCoverage() {
    return gulp.src('./coverage/lcov.info')
               .pipe(codecov());
}

gulp.task('coverage', gulp.series(uploadTestCoverage))

exports.coverage = uploadTestCoverage
