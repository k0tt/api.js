import gulp from 'gulp';
import * as config from '../config.js';

import {templates} from './templates'
import {webpack} from './webpack'
import './style'

function watchFiles() {
  gulp.watch(config.templates.files, templates)
  gulp.watch(config.js.files, webpack)
  gulp.watch(config.stylus.files, gulp.series('style'))
}

gulp.task('watch', gulp.parallel(watchFiles))

exports.watch = watchFiles
