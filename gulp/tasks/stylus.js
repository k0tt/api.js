import gulp from 'gulp';
import stylus from 'gulp-stylus';
import * as config from '../config.js';

function stylusCompile() {
    return gulp.src(config.stylus.files)
               .pipe(stylus(config.stylus.options))
               .pipe(gulp.dest(config.css.src));
}

gulp.task('stylus', gulp.series(stylusCompile))

exports.stylus = stylusCompile
