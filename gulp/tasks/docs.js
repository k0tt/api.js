/**
 * Generates documentation
 *     scss:   sassdoc
 */
import gulp from 'gulp'
import sassdoc from 'sassdoc'
import documentation from 'gulp-documentation'
import * as config from '../config.js'

function jsDocs() {
    return gulp
        .src(config.js.files)
        .pipe(documentation('html'))
        .pipe(gulp.dest(config.docs.js.src))
}

function sassDocs() {
    return gulp.src(config.scss.files)
               .pipe(sassdoc({
                   dest: config.docs.scss.src, 
                   verbose:true
               }))
}

gulp.task('docs', gulp.parallel(jsDocs))
