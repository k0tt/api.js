import gulp from 'gulp';
import pug from 'gulp-pug';
import * as config from '../config.js';

function pugify() {
    return gulp.src(config.templates.files, {base: "./"})
               .pipe(pug({
                    pretty: true
                }))
                .pipe(gulp.dest(config.templates.dest))
}

gulp.task('templates', gulp.series(pugify))

exports.templates = pugify
