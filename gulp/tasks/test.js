import gulp from 'gulp'
import ava from 'gulp-ava'
import codecov from 'gulp-codecov'
import dotenv from 'gulp-dotenv'
import * as config from '../config.js'

function runFunctionalTests() {
    return gulp.src(config.js.test.files.functional)
               .pipe(ava(config.js.test.options))
}

function runUnitTests() {
    return gulp.src(config.js.test.files.unit)
               .pipe(ava(config.js.test.options))
}

gulp.task('runFunctionalTests', gulp.series(runFunctionalTests))

gulp.task('runUnitTests', gulp.series(runUnitTests))

gulp.task('test', gulp.series(runFunctionalTests, runUnitTests))
