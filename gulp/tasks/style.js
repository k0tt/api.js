import gulp from 'gulp'

import {stylus} from './stylus'

gulp.task('style', gulp.series(stylus))
