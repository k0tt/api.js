import gulp from 'gulp'

import {webpack} from './webpack'
import {watch} from './watch'
import {templates} from './templates'

import './style'

gulp.task('default', 
    gulp.parallel(
        'style', 
        'webpack', 
        'templates', 
        'watch'
    ), () => {
        console.log('All default tasks completed')
    }
)
