'use strict';
import dotenv from 'dotenv'

dotenv.config({silent: true})

const base = process.env.STATIC_DIR
const dest = `${base}/dist`;

module.exports = {
  project_name: 'API',
  css: {
    src: `${base}/css`,
    files: ['*.css']
  },
  docs: {
    src: `${base}/docs`,
    js: {
        src: './docs/js'
    },
    scss: {
        src: './docs/scss'
    }
  },
  images: {
    src: `${base}/images`,
    dest: `${dest}/images`
  },
  js: {
    main: `${base}/js/index.js`,
    src: `${base}/js`,
    dest: `./dist`,
    files: [
        `${base}/js/*.js`,
        `${base}/js/modules/*.js`, 
        `${base}/js/modules/**/*.js`
    ],
    test: {
        src: `${base}/js/test`,
        files: {
            unit: [
                `${base}/js/test/unit/*.js`,
                `${base}/js/test/unit/**/*.js`
            ],
            functional: [
                `${base}/js/test/functional/*.js`,
                `${base}/js/test/functional/**/*.js`
            ]
        },
        options: {
            nyc: true,
            verbose: true
        }
    }
  },
  jsx: {
    src: `${base}/jsx`,
    dest: `${base}/js/jsx`,
    files: [`${base}/jsx/*.jsx`, `${base}/jsx/**/*.jsx`]
  },
  scss: {
    src: `${base}/scss`,
    dest: `${base}/css`,
    files: [`${base}/scss/*.{sass,scss}`]
  },
  stylus: {
    src: `${base}/stylus`,
    files: [
        `${base}/stylus/*.styl`,
        `${base}/stylus/**/*.styl`
    ],
    options: {}
  },
  templates : {
    files: [
        `${base}/templates/*.pug`,
        `${base}/templates/**/*.pug`
    ],
    dest: '.'
  }
};
