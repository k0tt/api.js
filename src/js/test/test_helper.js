import * as API from './../modules/api/api.js'

class Reset extends API.Base {}
class Car extends API.Base {}
class Ticket extends Car {}
class Owner extends Ticket {}

/** Helper config class */
class Config extends API.Base {
    is_resource = false
    ajax_request = {
        headers: {
            'test': 'testing'
        }
    }
}

class Derive extends Config {
    is_resource = true
}
class Apple extends Derive {}

export default {
    Reset, 
    Car, 
    Ticket, 
    Owner, 
    Config,
    Derive,
    Apple
}
