import bodyParser from 'body-parser'
import {config} from 'dotenv'
import express from 'express'
import Immutable from 'immutable'

import * as fixtures from './test_fixtures.js'

let app = express()
var cars = fixtures.generateCar(10)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

app.get('/resets', (req, res) => {
    cars = fixtures.generateCar(10)
})

app.get('/cars', (req, res) => {
    res.send(cars)
})

app.get('/cars/:id', (req, res) => {
    var car = cars.filter((car) => {
        return car.id == req.params.id
    })

    if(car.length) {
        return res.status(200).json(car)
    } else {
        return res.status(404).send('Not found')
    }
})

app.post('/cars', (req, res) => {
    cars.push(req.body)
    return res.status(200).send(req.body)
})

app.put('/cars/:id', (req, res) => {
    return res.status(200).send(req.body)
})

app.patch('/cars/:id', (req, res) => {
    return res.status(200).send(req.body)
})

app.delete('/cars/:id', (req, res) => {
    var id = req.params.id
    cars = cars.filter((car) => { return car.id != id })

    return res.status(200).json(cars)
})

var server = app.listen(8000, function () {

   var host = '127.0.0.1'
   var port = '8000'

   console.log("Test server listening at http://%s:%s", host, port)
})
