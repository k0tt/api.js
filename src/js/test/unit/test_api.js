import test from 'ava'
import Helpers from '../test_helper.js'

var car, ticket, owner, config, derive, apple

test.beforeEach(t => {
    car = new Helpers.Car(1)
    ticket  = new Helpers.Ticket(2, car)
    owner = new Helpers.Owner(3, ticket)
    config = new Helpers.Config()
    derive = new Helpers.Derive()
    apple = new Helpers.Apple(0, derive)
})

test.after('cleanup', t => {
})

test('Class name property should equal cars', t => {
    t.is('cars', car.process_class_name(car.constructor.name))
})

test('Test constructing url', t => {
    const url_with_id = car.construct_url()
    t.is('cars/1', url_with_id)
})

test('Test sub-resources 2 levels down', t => {
    const url_with_id = ticket.construct_url(2, ticket.parent)
    t.is('cars/1/tickets/2', url_with_id)
})

test('Test sub-resources 3 levels down', t => {
    const url_with_id = owner.construct_url(3, owner.parent)
    t.is('cars/1/tickets/2/owners/3', url_with_id)
})

test('Test encoding query data', t => {
	const data = {'testKey':'one'}
	const query_str = car.encodeQueryData(data)
	t.is('?testKey=one&', query_str)
})

test('test constructing url with query params', t => {
	const data = {
		'testKey' :'one',
		'testKey2':'two'
	}
        const carId = car.id
	car.id = 0
	const url = car.construct_url(0, {}, data)
	t.is('cars/?testKey=one&testKey2=two&',url)
        car.id = carId
})

test('test adding subresources', t => {
	ticket.sub_resources = ['search']
	const url = ticket.construct_url(2, ticket.parent)

	t.is('cars/1/tickets/2/search', url)
	ticket.sub_resources = []
})

test('test adding subresources where is_resource=false', t => {
    ticket.parent.is_resource = false
    const url = ticket.construct_url(2, ticket.parent)

    t.is('tickets/2', url)

    ticket.parent.is_resource = true
})

test('Test API class where is_resource = false', t => {
    t.is(false, config.is_resource)
    t.is(true, derive.is_resource)
})

test('Test nested resource which inherits from Config class (is_resource=false)', t => {
    const url = apple.construct_url(0, apple.parent)
    t.is('derives/apples', url)

    t.is('testing', apple.ajax_request.headers.test)
})

test('Test nested resource which inherits from Config class and includes sub_resources (is_resource = false)', t => {
    derive.is_resource = false
    derive.sub_resources = ['one','two']
    const url = derive.construct_url()
    t.is('one/two', url)
})

test('test merging options', t => {
    let default_request = {
        method: '',
        uri: '',
        body: {},
        json: true
    }
    let custom_request = { auth: {'user': 'this', 'pass': 'that'} }
    let merged = car.merge_options(default_request, custom_request)
    t.is(merged['auth']['user'],'this')
})
