import faker from 'faker'
import Immutable from 'immutable'
export {generateCar, generateTicket}


function generateCar(n)  {
    var i = 0
    var lis = []
    while(n > 0) {
        var map = {
            id: ++i,
            owner: faker.name.findName(),
            color: faker.internet.color(),
            license_number: faker.random.number()
        }
        lis.push(map)
        n--
    }
    return lis
}

function generateTicket() {
    return Immutable.Map({
        car: generateCar().id,
        message: faker.lorem.text,
        date: new Date()
    })
}

