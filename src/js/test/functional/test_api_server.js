import test from 'ava'
import Helpers from '../test_helper.js'
import {generateCar} from '../test_fixtures'
import dotenv from 'dotenv'

dotenv.config()

var car, ticket, owner, reset

test.before(t => {
    car = new Helpers.Car()
    ticket  = new Helpers.Ticket(car)
    owner = new Helpers.Owner(ticket)
    reset = new Helpers.Reset()
})

test.beforeEach(t => {
    car.clear()
    ticket.clear()
    owner.clear()
    reset.get()
})

test('It should GET data for car with id 1', async t => {
    var response = await car.get(1)
    t.is(response[0].id, 1)
})

test('It should POST new car', async t => {
    var new_car = generateCar(1)[0]
    new_car.id  = 11
    var response = await car.post(new_car)
    t.is(new_car.color, response.color)
})

test('It should delete car with id 1', async t => {
    var cars = await car.get()
    var response = await car.delete(1)
    t.is(cars.length - 1, response.length)
})

test('Fetching a resource should assign attributes from that resource to the API object', async t => {
    await car.fetch_resource(1)
    t.true(car.object.hasOwnProperty('color'))
})
