/* @flow */
import {XMLHttpRequest} from 'xmlhttprequest'

/** API Base class */
class Base {
    id: number
    parent: Base
    api_url: string
    is_resource: boolean
    constructor(
        id = 0, 
        parent = {}, 
        api_url = '',
        is_resource = true
    ) {
        this.api_url = process.env.API_URL || api_url
        this.id = id
        this.name = this.get_name()
        this.parent = parent
        this.storage = []
        this.url = ''
        this.object = {}
        this.ajax_request = undefined
        this.sub_resources = []
        this.is_resource = is_resource
    }

    /**
    * Resets instance
    * */
    clear() {
        this.object = {}
        this.id = 0
        this.url = ''
    }

    /**
    * Creates a query string from object
    * @param data
    * @returns query string
    * */
    encodeQueryData(data: Object):string {
        let query = '?'
        for (let key in data) {
          query += `${key}=${data[key]}&`
        }
        return query
    }

    /**
     * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
     * @param obj1
     * @param obj2
     * @returns obj3 - a new object based on obj1 and obj2
     */
    merge_options(obj1: Object, obj2: Object): Object {
        var obj3 = {};
        for (var attrname in obj1) { 
            obj3[attrname] = obj1[attrname]; 
        }
        for (var attrname in obj2) { 
            if(typeof(obj2[attrname]) === 'undefined' 
               && obj3[attrname]
            ) {
                delete obj3[attrname]
            }
            else {
                obj3[attrname] = obj2[attrname]; 
            }
        }
        return obj3;
    }

    /**
     * Fetch a resource and store its values
     * @param id - The id of the resource being stored
     * @returns - A resolved promise 
     * */
    fetch_resource(id: number): Promise {
        return this.get(id).then((data) => {
            let record = data[0]
            Object.assign(this.object, record)
        })
    }

    /**
     * Gets the name of the current class
     * */
    get_name(): string {
        return this.process_class_name(this.constructor.name)
    }

    /**
     * Gets resource path
     * @param id - The id of the resource
     * @returns  - The resource path
     * */
    get_resource_path(id: number = 0): string {
        if(!this.is_resource) {
            return ''
        }

        var url_str = `${this.name}`
        id = id || this.id
        if(id) {
            url_str += `/${id}`
        }

        return url_str
    }

    /**
     * Prepares resource ame for API naming conventions
     * @param name - The name of the resource
     * */
    process_class_name(name): string {
        return `${name}s`.toLowerCase()
    }

    /**
     * Recursively constructs an url from the current resource and all its
     * parent resources.
     * @param id - The id of the resource
     * @param parent - The parent instance of the resource
     * @param params - Object with query parameters
     * @returns - A constructed url
     * */
    construct_url(id: number = 0, parent: Object = {}, params: Object = {}): string {
        var self = this;
        var url_str = ''

        if(this.storage.length === 0) {
            this.storage.push(this.get_resource_path(id))
        }

        if(typeof(parent.id) !== 'undefined') {
            let pr = parent.get_resource_path()
            if(pr !== '') {
                this.storage.push(parent.get_resource_path())
            }
            return this.construct_url(0, parent.parent)
        }

        url_str = this.storage.reverse().join('/')

        if(this.sub_resources.length > 0) {

            if(this.is_resource) {
                url_str += '/'
            }

            url_str += `${this.sub_resources.join('/')}`
        }

        if (Object.keys(params).length > 0 &&
            params.constructor === Object) {
            url_str += `/${this.encodeQueryData(params)}`
        }
        
        this.storage = []
        return url_str
    }

    ajax_promise(request: Object): Promise {
        let areq = new Promise((resolve, reject) => {
            let xhReq = new XMLHttpRequest()
            xhReq.onload = () => {
                if(xhReq.status >= 200 && xhReq.status < 300) {
                    resolve(JSON.parse(xhReq.responseText))
                } else {
                    reject(JSON.parse(xhReq.responseText))
                }
            }
            xhReq.onerror = () => reject(xhReq.statusText)
            if(request.auth) {
                xhReq.open(
                    request.type,
                    request.url,
                    request.async,
                    request.auth.user,
                    request.auth.pass
                );
            } else {
                xhReq.open(request.type, request.url, request.async);
            }
            if (typeof(request.headers) !== 'undefined') {
                for(let [key, value] of Object.entries(request.headers)) {
                    xhReq.setRequestHeader(key, value);
                }
            }
            if(typeof request.data === 'undefined') {
                xhReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhReq.send(null);
            } else {
                xhReq.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
                xhReq.send(JSON.stringify(request.data))
            }
        })
        return areq
    }


    /**
     * Performs a request
     * @param method - Request method (GET | POST | PUT | PATCH | DELETE)
     * @param id     - The id of the resource (applies for PUT | PATCH | DELETE)
     * @param data   - Data being sent with the request
     * @param params - Available for GET requests. Sends data as query parameters in the URL
     * @returns      - Promise
     * */
    perform_request(method: string, id: number = 0, data: Object = {}, params: boolean = false): Promise {
        const construct_url = this.construct_url(
          id, 
          this.parent,
          (params) ? data : {}
        )
        let url = `${this.api_url}/${construct_url}/`
        let default_request = {
            type: method,
            url: url,
            headers: undefined,
            data: (params) ? undefined : data,
            async: true
        }

        if(this.ajax_request) {
            default_request = this.merge_options(
                default_request, this.ajax_request
            )
        }

        return this.ajax_promise(default_request)
    }

    /**
     * GET request
     * @param id    - The id of the resource
     * @param data  - The data being passed with the request
     * @returns - Promise
     * */
    get(id: number = 0, data: Object = {}, params: boolean = false): Promise {
        return this.perform_request(
            'GET', 
            id,
            data,
            params
        )
    }

    /**
     * POST request
     * @param data  - The data being passed with the request
     * @returns - Promise
     * */
    post(data: Object = {}): Promise {
        return this.perform_request(
            'POST',
            0,
            data
        )
    }

    /**
     * PUT request
     * @param id    - The id of the resource
     * @param data  - The data being passed with the request
     * @returns   - Promise
     * */
    put(id: number = 0, data: Object = {}): Promise {

        if(data === {}) {
            data = this.object
        }

        return this.perform_request(
            'PUT', 
            id,
            data
        )
    }

    /**
     * PATCH request
     * @param id    - The id of the resource
     * @param data  - The data being passed with the request
     * @returns     - Promise
     * */
    patch(id: number = 0, data: Object = {}): Promise {

        if(data === {}) {
            data = this.object
        }

        return this.perform_request(
            'PATCH', 
            id, 
            data
        )
    }

    /**
     * DELETE request
     * @param id - The id of the resource
     * @param data  - The data being passed with the request
     * @returns  - Promise
     * */
    delete(id: number = 0, data: Object = {}): Promise {
        return this.perform_request(
            'DELETE',
            id,
            data
        )
    }
}

export {Base}
