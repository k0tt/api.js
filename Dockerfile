FROM mhart/alpine-node:8

RUN npm install -g gulp
RUN npm install -g @babel/core @babel/node @babel/cli

RUN mkdir -p /app
ADD package.json /app
WORKDIR /app

RUN npm install

ADD . /app
