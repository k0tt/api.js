# API JS module

This module includes the basic boilerplate for interacting with RESTful API's.
The idea is that by following standard REST principles we will always end up with
the same url patterns. The only difference is the name of each seperate endpoint.

apidotjs implements the following HTTP request methods

*E.g.: Using cars as resource*

| Resource      | GET           | POST  | PUT | PATCH | DELETE |
| ------------- |:-------------:| -----:|----:|------:|-------:|
| /cars         | Read    | Create| Update/Replace| Update/Modify | Delete
| /cars/711    | Read | 405| Update/Replace| Update/Modify| Delete

Furthermore apidotjs abides to these standard REST principles

- Use nouns but no verbs
- GET method and query parameters should not alter the state
- Use plural nouns
- Use sub-resources for relations
- Use HTTP headers for serialization formats
