### Using the Base class

Any class interacting with an RESTful API endpoint extends the API.Base class.
This class includes a method for each of the request methods mentioned above.

Example

    import {API} from 'apidotjs/dist/api.min.js'


	class Car extends API.Base {}
	
	> const car = new Car()
	> car.name 
	'cars'
	> car.construct_url()
	'/cars/'
	> car.post({'color': 'green'})
	POST /cars/ (data {'color':'green'})
	
	
### Methods

##### GET

** arguments: **

- *id*: The id of the resource
- *data*: The data being sent with the request
- *params*: If set to true the key,values in data will be turned into query parameters instead of being send in the body of the request

##### POST

** arguments: **

- *data*: The data being sent with the request

##### PUT

** arguments: **

- *id*: The id of the resource
- *data*: The data being sent with the request

##### PATCH

** arguments: **

- *id*: The id of the resource
- *data*: The data being sent with the request

##### DELETE

** arguments: **

- *id*: The id of the resource



