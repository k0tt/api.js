### Sub-resources

Sub resources are created by further subclassing the class which inherits from the API.Base class.

Example

	class Ticket extends Car {}
	
	> const ticket = new Ticket(2, car)
	> ticket.object.extra_info
	'Some extra information about the ticket'
	> ticket.parent.object.color
	'green'
	
	/** Update information */
	> ticket.object.extra_info = 'no info'
	> ticket.patch()	
	PATCH /cars/1/tickets/2/
	
	> ticket.post({'extra_info':'blah'})
	405 METHOD NOT ALLOWED
	
	
If we are working with instances we need to clear the class in order to be able to post again

	> ticket.clear()
	> ticket.post({'some':'data'})
    > POST '/cars/1/tickets/'
    
### Adding additional API paths

You can add resources to your API path without having to subclass the parent.
This is done by adding to the sub_resources Array of the API.Base class.

Example

	const car = new Car()
	car.sub_resources = ['search']
	
	car.get(0, {'color':'blue'}, true)
	
	GET /cars/search/?color=blue