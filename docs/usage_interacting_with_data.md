### Interacting with data

A record can be fetched and stored by calling the fetch_resource method

Example

    const car = new Car()
    car.fetch_resource(1)
    GET /cars/1/


Now the attributes of car with id 1 are available through the object variable.

	> car.object.color
	'green'
