# Summary

* [Installation](installation.md)
* [Usage](usage.md)
	- [apidotjs library](usage_api_js_module.md)
	- [The Base class](usage_api_class.md)
	- [Interacting with data](usage_interacting_with_data.md)
	- [Sub resources](usage_sub_resources.md)
