const config = require('./gulp/config.js')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const Visualizer = require('webpack-visualizer-plugin')

module.exports = {
    target: 'node',
    context: __dirname + '/' + config.js.src,
    entry: {
        'api' : __dirname + '/' + config.js.main,
        'api.min' : __dirname + '/' + config.js.main
    },
    output: {
        path: __dirname + config.js.dest,
        filename: '[name].js',
        library: config.project_name,
        libraryTarget: 'commonjs'
    },
    resolve: {
        modules: [
            "node_modules",
            "web_modules",
            "bower_components"
        ],
        extensions: ['.js']
    },
    plugins: [
        new Visualizer({
            filename: './stats.html'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            _: 'underscore'
        }),
        new Dotenv({
            path: './.env', // if not simply .env
            safe: false // do not require env.example
        }),
        new webpack.DefinePlugin({
            'process.env': {
                // This has effect on the react lib size
                'NODE_ENV': JSON.stringify('production'),
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            include: /\.min\.js$/,
            mangle: {
	        except: ['API', 'Base']
	    },
            minimize: true,
            compress: {
                unused: true,
                dead_code: true,
                warnings: false,
                drop_debugger: true,
                drop_console: true,
		screw_ie8: true
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                }
            },{
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },{ 
                test: /\.html$/, 
                loader: "underscore-template-loader" 
            },{ 
                test: /\.css$/, 
                loader: "style-loader!css-loader" 
            },{ 
                test: /\.jpe?g$|\.gif$|\.png$|\.otf$|\.eot$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/, 
                loader: "file" 
            }
        ]
     }
 }
